# -*- coding: utf-8 -*-
import bs4
import grab
import channel_loader
from result_saver import Youtube
import datetime


def craw_youtube_subscriber():
    channels = channel_loader.get_channels_youtubeurls()
    today = datetime.datetime.today().strftime("%Y-%m-%d")
    for ch in channels:
        if ch['url'] is None:
            continue
        count = get_subscriber(ch['url'])
        ch['count'] = count
        print("{0} {1}".format(ch['title'], ch['count']))
        Youtube.save_subscribers(today, ch['hashid'], ch['count'])

    return channels


def get_subscriber(url):
    gr = grab.Grab()
    res = gr.go(url)
    soup = bs4.BeautifulSoup(res.body)
    count = soup.select("span.yt-subscription-button-subscriber-count-branded-horizontal.subscribed.yt-uix-tooltip")[0].text
    count = count.strip().replace(",", "")
    return int(count)

if __name__ == "__main__":
    craw_youtube_subscriber()
