# -*- coding: utf-8 -*-
import datetime
import json
import logging
import time
import sys
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support.ui import WebDriverWait
from channel_loader import get_channels_fabookurls
from result_saver import save_channels_follower

# logger = logger.getlogger("autoinsta")
# logger = logging.getLogger(__name__)
FACEBOOK_LOGIN_URL = "http://www.facebook.com/login"

logging.basicConfig(level=logging.INFO, filename="/home/ubuntu/facebook_follower_counter/autofacebook.log", format='%(asctime)s %(message)s')
#logging.debug('This message should go to the log file')
#logging.info('So should this')
#logging.warning('And this, too')
RDEBUG=False

def login(drv, userid, password):
    logging.info("try to login")
    drv.get(FACEBOOK_LOGIN_URL)
    lo_user = WebDriverWait(driver=drv, timeout=10).until(
        lambda drv: drv.find_element_by_xpath("//input[@name='email']"))
    lo_user.send_keys(userid)
    lo_pass = WebDriverWait(driver=drv, timeout=10).until(
        lambda drv: drv.find_element_by_xpath("//input[@name='pass']"))
    lo_pass.send_keys(password)
    time.sleep(3)
    lo_login_button = WebDriverWait(driver=drv, timeout=10).until(
        lambda drv: drv.find_element_by_css_selector("#loginbutton"))
    lo_login_button.click()
    time.sleep(3)


def collect_follower_count(link, new_driver=None):
    drv = new_driver
    time.sleep(3)
    drv.get(link)
    time.sleep(3)
    try:
        # 일반 개인의 페이지 일 때
        follower_count = WebDriverWait(driver=drv, timeout=10).until(
            lambda drv: drv.find_element_by_css_selector('a[href$="followers"]')).text
    except:
        # 페이지 형태 일 때
        follower_count = WebDriverWait(driver=drv, timeout=10).until(
            lambda drv: drv.find_element_by_css_selector('a[href$="likes"]')).text

    return int(follower_count.split(" ")[0].replace(",", ""))


def record_todays_follower(drv):
    if RDEBUG:
        facebook_urls = load_facebook_url_list()[:3]
    else:
        facebook_urls = load_facebook_url_list()

    for fa in facebook_urls:
        turl = fa['url']
        try:
            cnt = collect_follower_count(turl, new_driver=drv)
        except:
            logging.error("error {0}".format(fa['title']))
            fa['follower'] = "error"
            continue
        fa['follower'] = cnt
        logging.info("{0} {1}".format(fa['title'], cnt))

    today = datetime.datetime.today().strftime("%Y-%m-%d")
    save_result(today, facebook_urls)


def load_facebook_url_list():
    return get_channels_fabookurls()


def save_result(today, result):
    for item in result:
        if item['follower'] == 'error':
            continue

        save_channels_follower(today, item['hashid'], item['follower'])


def main(drv, userid, password):
    login(drv, userid, password)
    record_todays_follower(drv)


if __name__ == '__main__':
    #  ** this is how to run selenium server **
    # os.system("java -jar selenium-server-standalone-2.48.2.jar &")
    #  **
    # tl = "https://www.facebook.com/realtelon"
    userid = sys.argv[1]
    password = sys.argv[2]
    #http://172.31.26.90
    drv = webdriver.Remote(command_executor='http://172.31.26.90:4444/wd/hub',

                           desired_capabilities=DesiredCapabilities.FIREFOX)
    main(drv, userid, password)
