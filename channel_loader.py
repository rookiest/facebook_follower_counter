# -*- coding: utf-8 -*-
import requests
import json
import pprint


def get_channels():
    res = requests.get("http://api.rookiest.co.kr/apiroot/channels")
    channels = json.loads(res.content.decode('utf8'))['results']
    return channels


def get_channels_fabookurls():
    channels = get_channels()
    new_channels = list()
    # pprint.pprint(channels)
    for ch in channels:
        new_channel = {'title': ch['title'], 'hashid': ch[
            'hashid'], 'url': ch['facebookurl']}
        new_channels.append(new_channel)

    return new_channels


def get_channels_youtubeurls():
    channels = get_channels()
    new_channels = list()
    for ch in channels:
        new_channel = {'title': ch['title'], 'hashid': ch[
            'hashid'], 'url': ch['sns_url']}
        new_channels.append(new_channel)

    return new_channels
if __name__ == '__main__':
    # import pprint
    # pprint.pprint(
    # get_channels_fabookurls()
    pprint.pprint(get_channels_youtubeurls())
