# -*- coding: utf-8 -*-
import requests
import datetime

# IPADDRESS = '192.168.1.100:8080'
IPADDRESS = 'api.rookiest.co.kr'
TARGET_URL_TEMP = "http://{_ipaddr}/apiroot/channels/facebook/{_hashid}"
FOLLOWER_TARGET_URL_TEMP = "http://{_ipaddr}/apiroot/channels/facebook/{_hashid}/followers"

YOUTUBE_SUBS_SEND_URL_TEMP = "http://{_ipaddr}/apiroot/channels/youtube/{_hashid}"
YOUTUBE_SUBS_GET_URL_TEMP = "http://{_ipaddr}/apiroot/channels/youtube/{_hashid}/subscribers"


def save_channels_follower(datestring, hashid, count):
    target_url = TARGET_URL_TEMP.format(_ipaddr=IPADDRESS, _hashid=hashid)

    res = requests.post(target_url, data={'date': datestring, 'count': count})
    print(res.content)


def get_channels_follower(hashid):
    target_url = FOLLOWER_TARGET_URL_TEMP.format(_ipaddr=IPADDRESS, _hashid=hashid)
    res = requests.get(target_url)
    print(res.content)


class Youtube:

    @staticmethod
    def save_subscribers(datestring, hashid, count):
        print("save subscribers {0} {1} {2}".format(datestring, hashid, count))
        target_url = YOUTUBE_SUBS_SEND_URL_TEMP.format(_ipaddr=IPADDRESS, _hashid=hashid)
        res = requests.post(target_url, data={'date': datestring, 'count': count})
        print(res.content)

    @staticmethod
    def get_subscribers(hashid):
        target_url = YOUTUBE_SUBS_GET_URL_TEMP.format(_ipaddr=IPADDRESS, _hashid=hashid)
        res = requests.get(target_url)
        print(res.content)


def main():
    # today = datetime.datetime.today().strftime("%Y-%m-%d")
    # save_channels_follower("2015-11-01", "RnsrJ7L", 10001)
    # save_channels_follower("2015-11-02", "RnsrJ7L", 10031)
    # save_channels_follower("2015-11-03", "RnsrJ7L", 10161)
    # save_channels_follower("2015-11-04", "RnsrJ7L", 10341)
    # save_channels_follower(today, "RnsrJ7L", 307371)
    # requests.get()
    get_channels_follower("RnsrJ7L")


if __name__ == '__main__':
    main()
