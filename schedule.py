from plan import Plan
import sys
cron = Plan()

cron.command('record_follower', every='1.day', at='hour.23 minute.30')

if __name__ == '__main__':
    if sys.argv[1] == "write":
        cron.run('write')
    elif sys.argv[1] == "clear":
        cron.run("clear")
    else:
        print("error - invalid command")



